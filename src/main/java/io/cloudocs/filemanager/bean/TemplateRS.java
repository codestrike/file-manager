package io.cloudocs.filemanager.bean;

/**
 * Created by javofegus on 4/11/17.
 */
public class TemplateRS {
    private StoragePath storagePath;

    public StoragePath getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(StoragePath storagePath) {
        this.storagePath = storagePath;
    }
}
