package io.cloudocs.filemanager.service;

import io.cloudocs.filemanager.bean.StoragePath;
import io.cloudocs.filemanager.constant.FileConstant;
import io.cloudocs.filemanager.model.Attribute;
import io.cloudocs.filemanager.model.TemplateWrapper;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;

/**
 * Created by javofegus on 4/11/17.
 */
@Service
public class FileManagerService implements FileManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileManager.class);

    public TemplateWrapper prepareFile(String path) throws IOException {

        TemplateWrapper templateWrapper = new TemplateWrapper();
        FileInputStream inputStream = null;
        OPCPackage opcPackage;

        try {
            inputStream = new FileInputStream(path);
            opcPackage = OPCPackage.open(inputStream);
            XWPFDocument doc = new XWPFDocument(opcPackage);
            templateWrapper.setXwpfDocument(doc);

        } catch (IOException e) {
            LOGGER.error("prepareFile", e);
        } catch (InvalidFormatException e) {
            LOGGER.error("prepareFile", e);
        } finally {
            inputStream.close();
        }

        return templateWrapper;

    }

    @Override
    public StoragePath create(String fileName, String documentId, XWPFDocument doc) throws Exception {

        String path;

        path = FileConstant.DESTINATION_PATH + documentId + "/";

        StoragePath storagePath = new StoragePath();
        storagePath.setParentPath(path);
        File dir = new File(path);

        if (!dir.exists()) {
            boolean isDirectoryCreated = dir.mkdir();
            if (!isDirectoryCreated) {
                throw new Exception("Error creating directory.");
            }
        }

        String docFileName = fileName + ".docx";
        String docFullPath = path + docFileName;
        convertToDOCS(docFullPath, doc);

        String pdfFileName = fileName + ".pdf";
        String pdfFullPath = path + docFileName;
        convertToPDF(pdfFullPath,path + pdfFileName);

//        storagePath.setDocFile(docFileName);
        storagePath.setPdfFile(pdfFileName);
        return storagePath;
    }

    private void convertToDOCS(String path, XWPFDocument doc) throws IOException {

        FileOutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(path);
            doc.write(outputStream);

        } catch (IOException e) {
            LOGGER.error("convertToDOCS", e);
        } finally {
            outputStream.close();
            doc.close();
        }
    }

    private boolean convertToPDF(String docPath, String pdfPath) {

        boolean status = true;
        InputStream doc = null;
        XWPFDocument document = null;
        try {

            doc = new FileInputStream(new File(docPath));
            document = new XWPFDocument(doc);
            PdfOptions options = PdfOptions.create();
            OutputStream out = new FileOutputStream(new File(pdfPath));
            PdfConverter.getInstance().convert(document, out, options);

        } catch (Exception ex) {
            status = false;
            LOGGER.error("convertToPDF", ex);
        } finally {

            try {
                doc.close();
                document.close();
            } catch (IOException e) {
                LOGGER.error("convertToPDF", e);
            }

        }
        return status;
    }


    @Override
    public TemplateWrapper replaceFileText(String originPath, List<Attribute> attributes) {

        TemplateWrapper templateWrapper = null;

        try {

            templateWrapper = prepareFile(originPath);

            for (Attribute attr : attributes) {

                for (XWPFParagraph p : templateWrapper.getXwpfDocument().getParagraphs()) {
                    List<XWPFRun> runs = p.getRuns();
                    String text = "";
                    for (XWPFRun r : runs) {
                        text += r.text();
                    }
                    for (int i = runs.size() - 1; i > 0; i--) {
                        p.removeRun(i);
                    }
                    if (runs != null && runs.size()>0) {
                        XWPFRun run = runs.get(0);
                        String name = "[" + attr.getName() + "]";
                        if (text.contains(name)) {
                            text = text.replace(name, attr.getValue());
                            run.setText(text, 0);
                            break;
                        }
                    }
                }

                // for tables
                for (XWPFTable tbl : templateWrapper.getXwpfDocument().getTables()) {
                    for (XWPFTableRow row : tbl.getRows()) {
                        for (XWPFTableCell cell : row.getTableCells()) {
                            for (XWPFParagraph p : cell.getParagraphs()) {
                                for (XWPFRun r : p.getRuns()) {
                                    String text = r.getText(0);
                                    String name = "[" + attr.getName() + "]";
                                    if (text != null && text.contains(name)) {
                                        text = text.replace(name, attr.getValue());
                                        r.setText(text,0);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (IOException e) {
            LOGGER.error("replaceFileText", e);
        }

        return templateWrapper;
    }

}
