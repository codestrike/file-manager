package io.cloudocs.filemanager.service;

import io.cloudocs.filemanager.bean.StoragePath;
import io.cloudocs.filemanager.model.Attribute;
import io.cloudocs.filemanager.model.TemplateWrapper;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.IOException;
import java.util.List;

/**
 * Created by javofegus on 4/11/17.
 */
public interface FileManager {

    TemplateWrapper replaceFileText(String originPath, List<Attribute> attributes);
    StoragePath create(String fileFullName, String documentId, XWPFDocument doc) throws Exception;
}
