package io.cloudocs.filemanager.controller;

import io.cloudocs.filemanager.bean.StoragePath;
import io.cloudocs.filemanager.bean.TemplateRQ;
import io.cloudocs.filemanager.bean.TemplateRS;
import io.cloudocs.filemanager.constant.FileConstant;
import io.cloudocs.filemanager.model.TemplateWrapper;
import io.cloudocs.filemanager.service.FileManager;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by javofegus on 3/11/17.
 */
@RestController
@RequestMapping("filemanager")
public class DocumentController {

    @Autowired
    private FileManager fileManagerService;

    @PostMapping(value = "/templates", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> buildDocument(@RequestBody TemplateRQ templateRQ) throws Exception {


        // copy
        String path = FileConstant.DESTINATION_PATH + templateRQ.getDocumentId() + "/";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        String file = templateRQ.getName();
        String fileName = file;

        fileName = fileName + "_" + sdf.format(date);
        String docFileName = fileName + ".docx";
        String docFullPath = path + docFileName;

        File copyTo = new File(docFullPath);
        System.out.println(copyTo.toPath());
        FileUtils.copyFile(new File(templateRQ.getUrl()), new File(docFullPath));

        // replacing

        TemplateWrapper templateWrapper = fileManagerService.replaceFileText(
                copyTo.getAbsolutePath(), templateRQ.getAttributes());

        StoragePath storagePath = fileManagerService.create(
                fileName, templateRQ.getDocumentId(), templateWrapper.getXwpfDocument());

        TemplateRS templateRS = new TemplateRS();
        templateRS.setStoragePath(storagePath);

        if (templateRS.getStoragePath() != null) {
            return new ResponseEntity<>(templateRS, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(templateRS, HttpStatus.NOT_FOUND);
        }

    }
}
