package io.cloudocs.filemanager.model;

import java.util.List;

/**
 * Created by javofegus on 4/11/17.
 */
public class Template {

    private String id;
    private String type;
    private List<Attribute> attributes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }
}
