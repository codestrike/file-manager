package io.cloudocs.filemanager.model;

/**
 * Created by javofegus on 4/11/17.
 */
public class Attribute {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
