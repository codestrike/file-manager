package io.cloudocs.filemanager.model;

import org.apache.poi.xwpf.usermodel.XWPFDocument;

/**
 * Created by javofegus on 4/11/17.
 */
public class TemplateWrapper {
    private Template template;
    private XWPFDocument xwpfDocument;

    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public XWPFDocument getXwpfDocument() {
        return xwpfDocument;
    }

    public void setXwpfDocument(XWPFDocument xwpfDocument) {
        this.xwpfDocument = xwpfDocument;
    }
}
