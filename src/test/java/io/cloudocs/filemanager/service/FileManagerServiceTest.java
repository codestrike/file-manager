package io.cloudocs.filemanager.service;

import io.cloudocs.filemanager.model.Attribute;
import io.cloudocs.filemanager.model.TemplateWrapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by javofegus on 4/11/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FileManagerServiceTest {

    @Autowired
    FileManager fileManagerService;

    @Test
    public void shouldReplaceTextOnFile() {

        List<Attribute> attributes = new ArrayList<>();
        Attribute attribute = new Attribute();
        attribute.setName("Nombre");
        attribute.setValue("Javo");
        attributes.add(attribute);
        attribute = new Attribute();
        attribute.setName("Nombre del destinatario");
        attribute.setValue("Wills");
        attributes.add(attribute);

        String filePath = "plantilla01.docx";



//        try {
////            TemplateWrapper templateWrapper = fileManagerService.prepareFile("doc");
////            fileManagerService.replaceFileText(templateWrapper.getXwpfDocument(), attributes);
//
////            fileManagerService.create("newplantilla.docx",
////                    templateWrapper.getXwpfDocument());
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }
}
